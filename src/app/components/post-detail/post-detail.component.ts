import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params } from '@angular/router';
import { Post } from '../../models/post';
import { PostService } from '../../services/post.service';
import { UserService } from '../../services/user.service';


@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css'],
  providers: [PostService, UserService]
})
export class PostDetailComponent implements OnInit {

	public post: Post;
  public identity;

  constructor(
  	private _postService: PostService,
    private _userService: UserService,
  	private _router: Router,
	  private _route: ActivatedRoute
    

  	) { 
      this.identity = this._userService.getIdentity();
  }

  ngOnInit(): void {
  	this.getPosts();
  }


getPosts(){

this._route.params.subscribe(params =>{

	let id = +params['id'];
	

  this._postService.getPos(id).subscribe(
      response => {

        if(response.status == 'success'){
        	this.post = response.posts;
            console.log(this.post);
            
        }else{
         this._router.navigate(['inicio']);

        }
        
     
      },
      error => {
        
        console.log(error);
       // this._router.navigate(['inicio']);

      }

    );
   
  });

	}	
}


